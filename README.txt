== Overview ==
Image auto filter adds ALT tags for images in text, if they don't have it.

== Installation ==

Installing Image auto ALLT filter is pretty straightforward.
Just copy the Image auto ALLT filter directory
into your web site's /modules directory.

Then, activate the module by visiting http://www.example.com/admin/modules,
where example.com is the URL of your web site.
Or run drush en image_auto_alt_filter

== Configuration ==
Go to your text format settings and enable Image auto ALT filter.

== Important Information ==
Image auto ALT filter should be placed after
Limit allowed HTML tags filter, if it's enabled.
