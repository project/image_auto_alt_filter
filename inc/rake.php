<?php

/**
 * PHP implementation of Rapid Automatic Keyword Exraction.
 *
 * As described in:
 * ROSE, Stuart, et al. Automatic keyword extraction from individual documents.
 * Text Mining, 2010, 1-20.
 *
 * With help of Python implementation - <a
 * href="https://github.com/aneesha/RAKE">github.com/aneesha/RAKE</a>
 *
 * @version 0.1
 */
class ImgAltFilterRake {

  /**
   * Defining the stop words array.
   *
   * @var array
   */
  public $stopwordsArray;

  /**
   * Defining the wordLength variable.
   *
   * @var string
   */
  public $wordLength;

  /**
   * Defining the stop words pattern.
   *
   * @var string
   */
  public $stopwordsPattern;

  /**
   * Build stop words pattern from array given by parameter.
   *
   * @param string $stopwordsArray
   *   Array with stop words.
   * @param mixed $wordLength
   *   Variable holding word length.
   */
  public function __construct($stopwordsArray, $wordLength) {
    $this->stopwordsArray = $stopwordsArray;
    $this->wordLength = $wordLength;
    $this->stopwordsPattern = $this->buildStopwordsRegex();
  }

  /**
   * Extract key phrases from input text.
   *
   * @param string $text
   *   Input text.
   */
  public function extract($text) {
    $phrases_plain = self::splitSentences($text);
    $phrases = $this->getPhrases($phrases_plain);
    $scores = $this->getScores($phrases);
    $keywords = $this->getKeywords($phrases, $scores);
    arsort($keywords);
    return array_keys($keywords);
  }

  /**
   * Split sentences according to regex pattern.
   *
   * @param string $text
   *   Text to be splitted into sentences.
   */
  public static function splitSentences($text) {
    return preg_split('/[.?!,;\-"\'\(\)\\\X{2018}\X{2019}\X{2013}\t]+/u', $text);
  }

  /**
   * Splits phrase into words.
   *
   * @param string $phrase
   *   Phrase to be splitted into words.
   */
  public static function splitPhrase($phrase) {
    $words_temp = str_word_count($phrase, 1, '0123456789');
    $words = [];

    foreach ($words_temp as $w) {
      if ($w != '' and !(is_numeric($w))) {
        array_push($words, $w);
      }
    }
    return $words;
  }

  /**
   * Split sentences into phrases by loaded stop words.
   *
   * @param array $sentences
   *   Array of sentences.
   */
  private function getPhrases(array $sentences) {
    $w_length = $this->wordLength;
    $phrases_arr = [];
    foreach ($sentences as $s) {
      $phrases_temp = preg_replace($this->stopwordsPattern, '|', $s);
      $phrases = explode('|', $phrases_temp);
      foreach ($phrases as $p) {
        $p = strtolower(trim($p));
        $count_of_words = count(preg_split('/\s+/', $p));
        if ($p != '' && $count_of_words == $w_length) {
          array_push($phrases_arr, $p);
        }
      }
    }
    return $phrases_arr;
  }

  /**
   * Calculate score for each word.
   *
   * @param array $phrases
   *   Array containing individual phrases.
   */
  private function getScores(array $phrases) {
    $frequencies = [];
    $degrees = [];

    foreach ($phrases as $p) {
      $words = self::splitPhrase($p);
      $words_count = count($words);
      $words_degree = $words_count - 1;

      foreach ($words as $w) {
        $frequencies[$w] = (isset($frequencies[$w])) ? $frequencies[$w] : 0;
        $frequencies[$w] += 1;
        $degrees[$w] = (isset($degrees[$w])) ? $degrees[$w] : 0;
        $degrees[$w] += $words_degree;
      }
    }

    foreach ($frequencies as $word => $freq) {
      $degrees[$word] += $freq;
    }

    $scores = [];

    foreach ($frequencies as $word => $freq) {
      $scores[$word] = (isset($scores[$word])) ? $scores[$word] : 0;
      $scores[$word] = $degrees[$word] / (float) $freq;
    }

    return $scores;
  }

  /**
   * Calculate score for each phrase by words scores.
   *
   * @param array $phrases
   *   Array of phrases (optimally) returned by
   *   getPhrases() method.
   * @param array $scores
   *   Array of words and their scores returned by
   *   getScores() method.
   */
  private function getKeywords(array $phrases, array $scores) {
    $keywords = [];

    foreach ($phrases as $p) {
      $keywords[$p] = (isset($keywords[$p])) ? $keywords[$p] : 0;
      $words = self::splitPhrase($p);
      $score = 0;

      foreach ($words as $w) {
        $score += $scores[$w];
      }

      $keywords[$p] = $score;
    }
    return $keywords;
  }

  /**
   * Get loaded stop words and return regex containing each stop word.
   */
  private function buildStopwordsRegex() {
    $stopwordsArr = explode(",", $this->stopwordsArray);
    $stopwords_regex_arr = [];

    foreach ($stopwordsArr as $word) {
      array_push($stopwords_regex_arr, '\b' . trim($word) . '\b');
    }

    return '/' . implode('|', $stopwords_regex_arr) . '/i';
  }

}
